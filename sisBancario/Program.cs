﻿using System;

namespace sisBancario
{
    class Program
    {
        public static int senha = 0,  saldoCorrente = 0,  saldoPoupanca = 0,  contaCorrente = 0,  agenciaCorrente = 0;
        public static bool corrente = false, poupanca = false;

        static void Main(string[] args)
        {     
            bool sair = true;

            do {   
                Console.WriteLine("(1) Criar uma conta corrente.");
                Console.WriteLine("(2) Criar uma conta poupança.");
                Console.WriteLine("(3) Realizar deposito.");
                Console.WriteLine("(4) Realizar saque.");
                Console.WriteLine("(5) Consultar saldo.");
                Console.WriteLine("(6) Sair.");

                int opcao = int.Parse(Console.ReadLine());

                if (opcao == 1) {
                    criarConta();
                    corrente = true;
                } else if (opcao == 2) {
                    criarConta();
                    poupanca = true;
                } else if (opcao == 3) {
                    depositar();
                } else if (opcao == 4) {
                    saque();
                } else if (opcao == 5) {
                    consultarSaldo();
                } else if (opcao == 6) {
                    sair = false;
                } else {
                    Console.WriteLine("Opção inválida!");
                }
                 Console.Clear();
            } while (sair);

            Console.ReadKey();
        }

        public static void criarConta() {
            Random rnd = new Random();
    
            Console.WriteLine("Digite uma senha de 6 digitos: ");
            senha = int.Parse(Console.ReadLine());

            contaCorrente = rnd.Next(0000,9999);
            agenciaCorrente = rnd.Next(00000,99999);
            saldoCorrente = 0;
            saldoPoupanca = 0;

            Console.WriteLine($"Sua conta é: {contaCorrente}, e sua agência é: {agenciaCorrente}.");
            Console.WriteLine("Senha cadastrada com sucesso!");
            Console.ReadKey();
        }


        public static void depositar() {
            Console.WriteLine("(1) Depositar conta corrente?");
            Console.WriteLine("(2) Depositar conta Poupança?");

            int op = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite sua senha?");
            int senhaDigitada = int.Parse(Console.ReadLine());

            if (senhaDigitada == senha){
                if (op == 1) {
                    depositarCorrente();
                } else if (op == 2) {
                    depositarPoupanca();
                } else {
                    Console.WriteLine("Opção invalida, tente novamente.");
                    depositar();
                }
            } else {
                Console.WriteLine("Senha Invalida!");
                depositar();
            }
        }

        private static void depositarCorrente() {
            Console.WriteLine("Digite um valor a ser depositado: ");
            int deposito = int.Parse(Console.ReadLine());

            saldoCorrente += deposito;
        }
        
        private static void depositarPoupanca() {
            Console.WriteLine("Digite um valor a ser depositado: ");
            int deposito = int.Parse(Console.ReadLine());

            saldoPoupanca += deposito;
        }

        public static void saque() {
            Console.WriteLine("(1) Sacar conta corrente?");
            Console.WriteLine("(2) Sacar conta poupança?");

            int op = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite sua senha?");
            int senhaDigitada = int.Parse(Console.ReadLine());

            if (senhaDigitada == senha) {
                if (op == 1) {
                    saqueCorrente();
                } else if(op == 2) {
                    saquePoupanca();
                } else {
                    Console.WriteLine("Opção invalida, tente novamente.");
                    saque();
                }
            } else {
                Console.WriteLine("Senha Invalida!");
                saque();
            }
        }
        
        private static void saqueCorrente() {
            Console.WriteLine("Digite um valor a ser retirado: ");
            Console.WriteLine("Para sair digite -1. ");
            int valor = int.Parse(Console.ReadLine());

            if (valor != -1) {
                if (valor <= saldoCorrente)
                    saldoCorrente -= valor;
                else {
                    Console.WriteLine("Saldo de conta corrente inferior ao saldo desejado. Tente outro valor.");
                    Console.ReadKey();
                    saqueCorrente();
                }
            }
        }

        private static void saquePoupanca() {
            Console.WriteLine("Digite um valor a ser retirado: ");
            Console.WriteLine("Para sair digite -1. ");
            int valor = int.Parse(Console.ReadLine());

            if (valor != -1) {
                if (valor <= saldoPoupanca)
                    saldoPoupanca -= valor;
                else {
                    Console.Clear();
                    Console.WriteLine("Saldo de conta poupança inferior ao saldo desejado ( "+valor+ " )"+". Tente outro valor.");
                    Console.ReadKey();
                    saquePoupanca();
                }
            }
        }

        public static void consultarSaldo() {
            Console.WriteLine("(1) Consultar saldo conta corrente!");
            Console.WriteLine("(2) Consultar saldo conta poupança!");

            int opcaoSaldo = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite sua senha?");
            int senhaDigitada = int.Parse(Console.ReadLine());

            if (senhaDigitada == senha) {
                 Console.Clear();
                if (opcaoSaldo == 1) {
                    Console.WriteLine("O saldo da sua conta corrente é: " + saldoCorrente);
                } else {
                    Console.WriteLine("O saldo da sua conta poupança é: " + saldoPoupanca);
                }
            } else {
                Console.WriteLine("Senha Invalida!");
                Console.ReadKey();
                consultarSaldo();
            }

            Console.ReadKey();
        }
    }
}
